# Phantomjs Service

* This is a simple service that periodically scraps a webpage of your choice and saves it as an image.

* Open the schedular.js file to get a better understanding and flow of the whole application

* To start the service make sure you have node installed. At least version 8. Then go to your command prompt or terminal and type:
'node schedular.js' to start the service.