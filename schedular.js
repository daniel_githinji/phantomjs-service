var schedule = require('node-schedule');

var scrapeWebPage = function (fileName) {
    var bash_command = 'phantomjs rasterize.js http://ariya.github.io/svg/tiger.svg ' + fileName;

    var exec = require('child_process').exec;

    var child = exec(bash_command, (error, stdout, stderr) => {
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
        if (error !== null) {
            console.log(`exec error: ${error}`);
        }
    });
}

var counter = 0;

schedule.scheduleJob('* * * * *', function(){
    console.log('Started.............');

    var fileName =  'images/' + counter + 'tiger.png';
    scrapeWebPage(fileName);

    counter++;

    console.log('Done................');
});